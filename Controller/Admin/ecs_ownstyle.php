<?php

/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

namespace Ecs\OwnStyle\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminController;
use OxidEsales\Eshop\Core\Registry;

class ecs_ownstyle extends AdminController
{
    protected $_sThisTemplate = "ecs_ownstyle.tpl";
    const CONFIG_MODULE_NAME  = 'module:ecs_ownstyle';

    public function render()
    {
        $oConf                                      = Registry::getConfig();
        $this->_aViewData['ecs_ownstyle_styles']    = $oConf->getShopConfVar('ecs_ownstyle_styles');
        $this->_aViewData['ecs_ownstyle_scripts']   = $oConf->getShopConfVar('ecs_ownstyle_scripts');
        $this->_aViewData['ecs_ownstyle_styleson']  = $oConf->getShopConfVar('ecs_ownstyle_styleson');
        $this->_aViewData['ecs_ownstyle_scriptson'] = $oConf->getShopConfVar('ecs_ownstyle_scriptson');
        $this->_aViewData['ecs_ownstyle_htmlmin']   = $oConf->getShopConfVar('ecs_ownstyle_htmlmin');
        $this->_aViewData['ecs_ownstyle_allimp']    = $oConf->getShopConfVar('ecs_ownstyle_allimp');
        return parent::render();
    }

    public function save()
    {
        $oConf   = Registry::getConfig();
        $aParams = Registry::getConfig()->getRequestParameter("editval");
        $sShopId = $oConf->getShopId();
        $oConf->saveShopConfVar('str', 'ecs_ownstyle_styles', $aParams['ecs_ownstyle_styles'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('str', 'ecs_ownstyle_scripts', $aParams['ecs_ownstyle_scripts'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('bool', 'ecs_ownstyle_styleson', $aParams['ecs_ownstyle_styleson'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('bool', 'ecs_ownstyle_scriptson', $aParams['ecs_ownstyle_scriptson'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('bool', 'ecs_ownstyle_htmlmin', $aParams['ecs_ownstyle_htmlmin'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('bool', 'ecs_ownstyle_allimp', $aParams['ecs_ownstyle_allimp'], $sShopId, self::CONFIG_MODULE_NAME);
    }

}
