<?php

/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

namespace Ecs\OwnStyle\Core;

use \OxidEsales\Eshop\Core\Registry;
ini_set("pcre.recursion_limit", "16777"); // 8MB stack. *nix

class Output extends Output_parent
{
    // FROM: http://stackoverflow.com/questions/5312349/minifying-final-html-output-using-regular-expressions-with-codeigniter?answertab=active#tab-top
    // Set PCRE recursion limit to sane value = STACKSIZE / 500
    // ini_set("pcre.recursion_limit", "524"); // 256KB stack. Win32 Apache

    protected function process_data_jmr1($text)
    {
        $re = '%# Collapse whitespace everywhere but in blacklisted elements.
        (?>             # Match all whitespaces other than single space.
          [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
        | \s{2,}        # or two or more consecutive-any-whitespace.
        ) # Note: The remaining regex consumes no text at all...
        (?=             # Ensure we are not in a blacklist tag.
          [^<]*+        # Either zero or more non-"<" {normal*}
          (?:           # Begin {(special normal*)*} construct
            <           # or a < starting a non-blacklist tag.
            (?!/?(?:textarea|pre|script)\b)
            [^<]*+      # more non-"<" {normal*}
          )*+           # Finish "unrolling-the-loop"
          (?:           # Begin alternation group.
            <           # Either a blacklist start tag.
            (?>textarea|pre|script)\b
          | \z          # or end of file.
          )             # End alternation group.
        )  # If we made it here, we are not in a blacklist tag.
        %Six';
        $ret = preg_replace($re, " ", $text);
        if ($ret === null) {
            return $text;
        }

        return $ret;
    }

    public function process($sValue, $sClassName)
    {
        $ret = parent::process($sValue, $sClassName);
        if (!isAdmin() && $sClassName != "oxemail" && Registry::getConfig()->getConfigParam('ecs_ownstyle_htmlmin')) {
            $ret = $this->process_data_jmr1($sValue);
        }
        return $ret;
    }
}
