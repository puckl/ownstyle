eComStyle.de::OwnStyle
==========================


Composer name
---------------
ecs/ownstyle


Shopversionen  
-------------
OXID eShop CE/PE 6  


Installation
--------------

1. Erstellen Sie via SSH-Client eine Verbindung mit dem Server, auf dem Ihr OXID eShop liegt.
Wechseln Sie in das OXID-Projektverzeichnis, in dem sich die Datei composer.json sowie die source- und vendor-Ordner befinden.
Führen Sie dort folgenden Befehl aus:

    composer require ecs/autoload

2. Kopieren Sie den entpackten Modulordner **OwnStyle** per FTP in Ihren OXID eShop nach source/modules/ecs/.

Es sollte sich dann folgende Ordnerstruktur ergeben:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  source
  └── modules
      └── ecs
          ├── Autoload
          └── OwnStyle
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Loggen Sie sich anschließend in Ihren Shop-Admin ein und aktivieren das neue Modul unter Erweiterungen/Module.

Hinweis:
Wenn Sie mehrere eComStyle.de - Module für OXID 6 nacheinander installieren möchten, müssen Sie Punkt 1. nur bei der ersten Modulinstallation ausführen.
Bei allen weiteren eComStyle.de Module können Sie mit Punkt 2. beginnen.



Lizenzinformationen
-------------------
Please retain this copyright header in all versions of the software

Copyright (C) Josef A. Puckl | eComStyle.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see {http://www.gnu.org/licenses/}.



Sonstige Wünsche
----------------
Sehr gerne sind wir Ihnen bei allen Aufgaben rund um Ihren Onlineshop behilflich!
Fragen Sie bitte jederzeit einfach an: <https://ecomstyle.de/kontakt/ >