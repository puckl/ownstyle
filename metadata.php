<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$sMetadataVersion = '2.0';
$aModule = array(
    'id'            => 'ecs_ownstyle',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>OwnStyle</i>',
    'description'   => '<i>Eigene Styles und Scripte bequem im Theme integrieren</i>>',
    'thumbnail'     => 'ecs.png',
    'version'       => '2.0.0',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'support@ecomstyle.de',
    'url'           => 'https://ecomstyle.de',
    'extend'        => array(
        \OxidEsales\Eshop\Core\Output::class => Ecs\OwnStyle\Core\Output::class,
    ),
    'controllers' => array(
        'ecs_ownstyle'  => Ecs\OwnStyle\Controller\Admin\ecs_ownstyle::class,
     ),
    'templates'    => array(
        'ecs_ownstyle.tpl' => 'ecs/OwnStyle/views/admin/tpl/ecs_ownstyle.tpl'
     ),
    'blocks' => array(
        array('template' => 'layout/base.tpl', 'block' => 'head_css', 'file' => '/views/blocks/ecs_ownstyles.tpl'),
    ),
    'settings' => array(
        array('group'   => 'ecs_donotchange', 'name' => 'ecs_ownstyle_allimp',      'type' => 'bool', 'value' => true),
        array('group'   => 'ecs_donotchange', 'name' => 'ecs_ownstyle_htmlmin',     'type' => 'bool', 'value' => false),
        array('group'   => 'ecs_donotchange', 'name' => 'ecs_ownstyle_styles',      'type' => 'str'),
        array('group'   => 'ecs_donotchange', 'name' => 'ecs_ownstyle_scripts',     'type' => 'str'),
        array('group'   => 'ecs_donotchange', 'name' => 'ecs_ownstyle_styleson',    'type' => 'bool', 'value' => true),
        array('group'   => 'ecs_donotchange', 'name' => 'ecs_ownstyle_scriptson',   'type' => 'bool', 'value' => true),
    ),
    'events' => array(
        'onActivate'   => '\Ecs\OwnStyle\Core\Events::onActivate',
        'onDeactivate' => '\Ecs\OwnStyle\Core\Events::onDeactivate',
    ),
);
