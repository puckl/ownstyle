<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
$sLangName  = "Deutsch";
$aLang = array(
    'charset'                               => 'UTF-8',
    'ecsmenu'                               => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'ecs_ownstyle_menu'                     => 'Eigene Styles',
    'SHOP_MODULE_GROUP_ecs_donotchange'     => 'Einstellmöglichkeiten in der linken Adminnavigation unter <i>eComStyle.de > Eigene Styles</i> !
<style type="text/css">
<!--
.groupExp dl,input{display:none!important}
-->
</style>',
);