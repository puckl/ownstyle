[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) 2016  Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
*}]
[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box=" "}]
[{assign var="oConf" value=$oViewConf->getConfig()}]
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<body>
<style type="text/css">
<!--
@media (min-width: 1200px){
.container {
    width: 95%;
}
}
-->
</style>
    <div class="container">
        [{ if $readonly }] [{assign var="readonly" value="readonly disabled"}] [{else}] [{assign var="readonly" value=""}] [{/if}]
        <form name="transfer" id="transfer" action="[{ $oViewConf->getSelfLink() }]" method="post">     [{ $oViewConf->getHiddenSid() }]
            <input type="hidden" name="oxid" value="[{ $oxid }]">
            <input type="hidden" name="cl" value="ecs_ownstyle">
            <input type="hidden" name="fnc" value="">
            <input type="hidden" name="actshop" value="[{$oViewConf->getActiveShopId()}]">
            <input type="hidden" name="updatenav" value="">
            <input type="hidden" name="editlanguage" value="[{ $editlanguage }]">
        </form>
        <form name="myedit" id="myedit" action="[{ $oViewConf->getSelfLink() }]" method="post">     [{ $oViewConf->getHiddenSid() }]
            <input type="hidden" name="cl" value="ecs_ownstyle">
            <input type="hidden" name="fnc" value="">
            <input type="hidden" name="language" value="[{ $actlang }]">
            <div class="row">
                <div class="col-xs-6">
                    <h3>Eigene Styles (CSS):</h3>
                    <div class="">
                        <span class="text-muted">&lt;style type="text/css"&gt;</span>
                        <textarea class="form-control txtfield" style="min-width:100%;min-height:500px;font-size:15px;line-height:1.5em;" name="editval[ecs_ownstyle_styles]" [{ $readonly }]>[{$ecs_ownstyle_styles}]</textarea>
                        <span class="text-muted">&lt;/style&gt;</span>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="editval[ecs_ownstyle_styleson]" value="1" [{if $ecs_ownstyle_styleson}]checked="checked"[{/if}]> Styles verwenden
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="editval[ecs_ownstyle_allimp]" value="1" [{if $ecs_ownstyle_allimp}]checked="checked"[{/if}]> Jeder OwnStyle "! important" (wird bevorzugt)
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="editval[ecs_ownstyle_htmlmin]" value="1" [{if $ecs_ownstyle_htmlmin}]checked="checked"[{/if}]> Minify html-Output (Leerzeichen aus dem <a href="view-source:[{$oConf->getShopURL()}]" target="_blank" >Seitenquelltext</a> entfernen)
                        </label>
                    </div>
                </div>
                <div class="col-xs-6">
                    <h3>Eigene Scripte:</h3>
                    <div class="">
                        <span class="text-muted">&lt;script type="text/javascript"&gt;</span>
                        <textarea class="form-control txtfield" style="min-width:100%;min-height:500px;font-size:15px;line-height:1.5em;" name="editval[ecs_ownstyle_scripts]" [{ $readonly }]>[{$ecs_ownstyle_scripts}]</textarea>
                        <span class="text-muted">&lt;/script&gt;</span>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="editval[ecs_ownstyle_scriptson]" value="1" [{if $ecs_ownstyle_scriptson}]checked="checked"[{/if}]> Scripte verwenden
                        </label>
                    </div>
                </div>
            </div>
            <br>
            <input type="submit" class="edittext [{* btn btn-primary btn-lg  *}]btn-block" id="oLockButton" value=" [{ oxmultilang ident="GENERAL_SAVE" }] " onclick="Javascript:document.myedit.fnc.value='save'" [{ $readonly }]>
            <br>
            <span style="float:right;">Proudly powered by <a href="https://ecomstyle.de" target="_blank"><strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong></a></span>
        </form>
    </div>
</body>