[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) 2016  Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
*}]
[{assign var="oConf" value=$oViewConf->getConfig()}]
[{assign var="ownstyles" value=$oConf->getConfigParam('ecs_ownstyle_styles')}]
[{assign var="ownscripts" value=$oConf->getConfigParam('ecs_ownstyle_scripts')}]
[{assign var="ownstyleson" value=$oConf->getConfigParam('ecs_ownstyle_styleson')}]
[{assign var="ownscriptson" value=$oConf->getConfigParam('ecs_ownstyle_scriptson')}]
[{assign var="important" value=$oConf->getConfigParam('ecs_ownstyle_allimp')}]
[{* ownStyles *}]
[{if $ownstyleson}]
    [{assign var="ownstyles" value=$ownstyles|strip|replace:"; ":";"|replace:" }":"}"|replace:"} ":"}"|replace:", ":","|replace:": ":":"|replace:" {":"{"|replace:"{ ":"{"|replace:"  ":""|replace:"  ":""}]
    [{if $important}]
        [{assign var="ownstyles" value= $ownstyles|replace:"!important":""|replace:";":"!important;"|replace:"!important!important":"!important"}]
    [{/if}]
    [{capture append="oxidBlock_pageHead"}][{strip}]
        <style type="text/css">[{$ownstyles}]</style>
    [{/strip}][{/capture}]
[{/if}]
[{* OwnScripts *}]
[{if $ownscriptson}]
[{capture append="oxidBlock_pageScript"}]
    [{capture assign=ownscript}]
        $( "body" ).addClass( "body_[{$oViewConf->getTopActiveClassName()}]" );
        [{$ownscripts}]
    [{/capture}]
    [{oxscript add=$ownscript}]
[{/capture}]
[{/if}]
[{$smarty.block.parent}]